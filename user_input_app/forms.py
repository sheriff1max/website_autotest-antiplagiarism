from django.forms import ModelForm, TextInput, PasswordInput
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class PersonInput(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', ]

        widgets = {
            'username': TextInput(attrs={
                'class': 'wrap-input100 validate-input',
                'placeholder': 'Логин',
            }),
            'password': PasswordInput(attrs={
                'class': 'wrap-input100 validate-input',
                'placeholder': 'Пароль',
                'type': 'password',
                'id': 'password-input',
            }),
        }


class PersonRegister(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'first_name', 'last_name', ]

        widgets = {
            'first_name': TextInput(attrs={
                'class': 'wrap-input100 validate-input',
                'placeholder': 'Имя',
                'required': 'True',
            }),
            'last_name': TextInput(attrs={
                'class': 'wrap-input100 validate-input',
                'placeholder': 'Фамилия',
                'required': 'True',
            }),
            'username': TextInput(attrs={
                'class': 'wrap-input100 validate-input',
                'placeholder': 'Логин',
                'unique': 'True',
            }),
        }

    def __init__(self, *args, **kwargs):
        super(PersonRegister, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = PasswordInput(attrs={'class': 'wrap-input100 validate-input', 'placeholder': 'Пароль'})
        self.fields['password2'].widget = PasswordInput(attrs={'class': 'wrap-input100 validate-input', 'placeholder': 'Повторите пароль'})
