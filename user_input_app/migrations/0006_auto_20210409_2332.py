# Generated by Django 3.1.5 on 2021-04-09 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_input_app', '0005_auto_20210409_2323'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='login',
        ),
        migrations.AlterField(
            model_name='person',
            name='username',
            field=models.CharField(max_length=20, unique=True, verbose_name='Логин'),
        ),
    ]
