from django.contrib import admin
from django.contrib.auth.models import Group
from .models import PracticalTaskModel, PracticalUserWorkModel


class PracticalTaskModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'course', 'subject', 'number', 'variant', )
    list_filter = ('course', 'subject', 'number', 'variant',)
    search_fields = ('id', )

    # Небольшое украшение форм
    def render_change_form(self, request, context, *args, **kwargs):
        form_instance = context['adminform'].form
        form_instance.fields['text_code'].widget.attrs['placeholder'] = 'Пример 1 (ПРОБЕЛЫ ОБЯЗАТЕЛЬНЫ):\n2; 10 = 12  (2 и 10 аргументы, 12 результат функции)\n0; 5 = 5\n\nПример 2:\n5; = 20; 25  (В этом случае справа будет ВСЕГДА СПИСОК [20, 25]. Ввод без скобок)\n\nПример 3:\n3; 0 = YES  (Результат - строка. Ввод без ковычек)'
        form_instance.fields['text_code'].widget.attrs['style'] = 'resize:none;'
        form_instance.fields['condition'].widget.attrs['style'] = 'resize:none;'
        return super().render_change_form(request, context, *args, **kwargs)


class PracticalUserWorkModelAdmin(admin.ModelAdmin):
    list_filter = ('date', 'course', 'subject', 'number', 'variant',)
    list_display = ('id', 'user', 'course', 'subject', 'number', 'variant', )
    readonly_fields = ('user', 'course', 'subject', 'number', 'variant', )
    exclude = ['practical', 'mark', ]


admin.site.site_header = 'Панель преподавателя'
admin.site.register(PracticalTaskModel, PracticalTaskModelAdmin)
admin.site.register(PracticalUserWorkModel, PracticalUserWorkModelAdmin)
admin.site.unregister(Group)
